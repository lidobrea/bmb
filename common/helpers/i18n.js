module.exports = {
    LOADING_STATIC: 'Loading static data',
    LOAD: 'Load CSV',
    DATA_REMOVED: 'Server removed data'
};