module.exports = {
    SERVER: {
        PORT: process.env.PORT || '8080'
    },
    EVENTS: {
        ADD: 'add',
        CHANGE: 'change',
        READY: 'ready',
        ERROR: 'error',
        REMOVE: 'unlink'
    },
    FILENAMES: {
        MARTOR: 'martor',
        GRAFIC: 'grafic'
    },
    ENV: {
        DEV: 'development',
        PROD: 'production'
    },
    SOCKET: {
        ON_CONNECT: 'connect',
        ON_DISCONNECT: 'disconnect',
        ON_CLOSE: 'ON_CLOSE',
        ON_DATA_REQUEST: 'ON_DATA_REQUEST',
        ON_DATA_FILE: 'ON_DATA_FILE',
        ON_DATA: 'ON_DATA',
        ON_DATA_REMOVED: 'ON_DATA_REMOVED',
        ON_COMMAND: 'ON_COMMAND',
        STATUS: 'STATUS'
    },
    ACTIONS: {
        LONG: 'LONG',
        SHORT: 'SHORT',
        NEUTRAL: 'NEUTRAL'
    },
    INDEX: {
        ID: 'OrderId',
        ISO_DATE: 'ISODate',
        DATE: 'Data/ora',
        MOTIVATION: 'Motivatie',
        MID_PRICE: 'Pret mediu',
        STATE: 'Stare pozitie',
        ACTION: 'Actiune',
        PROFIT: 'Profit',
        MOTIVATIONS: {
            TURA: 'Tura',
            IN: 'Intrare',
            OUT: 'Iesire',
            STANDBY: 'STANDBY',
            PROTECTS: 'PROTECTS',
            FAST_TAKE_PROFIT: 'FAST TAKE PROFIT',
            TAKE_PROFIT: 'TAKE PROFIT',
            BOUNCE_BACK: 'BOUNCE BACK',
            FAST_BOUNCE_BACK: 'FAST BOUNCE BACK',
            FAST_CORRECTION: 'FAST CORRECTION',
            FAST_STOP_LOSS: 'FAST STOP LOSS',
            STOP_LOSS: 'STOP LOSS',
            STOPLOSS: 'STOP_LOSS',
            CLOSURE: 'CLOSURE',
            CANCEL: 'CANCEL',
            DOUBLE: 'DUBLURA',
            DOUBLE_EMPTY: '      ',
        },
        ACTIONS: {
            BUY: 'BUY',
            SELL: 'SELL',
            OPEN: 'OPEN',
            CLOSE: 'CLOSE',
        },
        STATES: {
            LONG: 'LONG',
            SHORT: 'SHRT'
        },
        MARKERS: {
            TAGS: {
                TURA: 'TURA',
                SIMULAT: 'SIMULAT',
                REAL: 'REAL',
                L: 'L',
                S: 'S',
                B: 'B',
                C: 'C',
                O: 'O'
            },
        },
    },
    COLORS: {
        BLACK: '#000',
        CHART: '#7cb5ec',
        BLUE: '#0000FF',
        WHITE: '#FFF',
        WHITE_GREY: '#C0C0C0',
        DARK_GREY: '#808080',
        RED_BLACK: '#370000',
        YELLOW: '#FFFF00',
        LIGHT_YELLOW: '#FFE385',
        GREEN: '#00FF00',
        ORANGE: '#FFA500',
        RED: '#FF0000'
    },
    DATE_FORMATS: {
        LT: 'HH:mm',
        LTS: 'H:mm:ss:SSS',
        L: 'DD-MM-YYYY',
        LL: 'D MMMM YYYY',
        LLL: 'D MMMM YYYY HH:mm',
        CUSTOM: `D MMMM 'YY h:mm:ss:SSS`,
        LLLL: 'dddd D MMMM YYYY HH:mm'
    },
    DEBOUNCE_RATE: 150,
    CHART: {
        ZOOM: {
            M1: '1M',
            M5: '5M',
            M30: '30M',
            H1: '1H',
            ALL: 'All'
        }
    }
};