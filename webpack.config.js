const path = require('path');
process.traceDeprecation = true;

module.exports = function (env) {
    return {
        context: path.join(__dirname, 'client'),
        entry: require('./webpack/entry'),
        output: require('./webpack/output')(env),
        module: require('./webpack/module')(env),
        plugins: require('./webpack/plugins')(env),
        resolve: require('./webpack/resolve'),
        devServer: require('./webpack/dev-server'),
        // devtool: require('./webpack/devtool')(env),
        node: {
            console: true,
            fs: 'empty',
            net: 'empty',
            tls: 'empty'
        }
    }
};