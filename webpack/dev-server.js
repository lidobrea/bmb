const path = require('path');
const ROOT = path.join(path.dirname(__filename), '/../');

module.exports = {
    contentBase: path.join(ROOT, 'dist'),
    clientLogLevel: 'info',
    port: 3000,
    compress: true,
    inline: true,
    historyApiFallback: false,
    watchOptions: {
        aggregateTimeout: 300,
        poll: 1000,
        ignored: /node_modules/
    }
};
