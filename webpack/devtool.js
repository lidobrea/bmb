module.exports = function (env) {
    const isProd = (env.hasOwnProperty('production') && env['production'] === true);
    return isProd ? 'source-map' : 'eval-source-map';
};