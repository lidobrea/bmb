module.exports = {
    main: './src/index.js',
    vendor: ['jquery', 'moment', 'lodash', 'socket.io-client', 'request-promise']
};