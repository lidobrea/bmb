const path = require('path');
const ROOT = path.join(path.dirname(__filename), '/../');

module.exports = {
    alias: {
        Common: path.resolve(ROOT, 'common'),
        Client: path.resolve(ROOT, 'client'),
        Libs: path.resolve(ROOT, 'libs')
    },
    modules: ['node_modules'],
    extensions: ['.js', '.less', '.json', '.ejs', '.html']
};
