const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanCSSPlugin = require('less-plugin-clean-css');

module.exports = function (env) {
    return {
        rules: [
            {
                test: /\.less$/,
                use: ExtractTextPlugin.extract({
                    use: [{
                        loader: 'css-loader',
                        options: {
                            sourceMap: true
                        }
                    }, {
                        loader: 'less-loader', options: {
                            sourceMap: true,
                            plugins: [
                                new CleanCSSPlugin({ advanced: true })
                            ]
                        }
                    }],
                    fallback: 'style-loader'
                })
            },
            {
                test: /\.json$/,
                use: 'json-loader'
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|server)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env'],
                        sourceMap: true,
                        plugins: ['transform-runtime']
                    }
                }
            }
        ]
    }
};