const webpack = require('webpack');
const { SERVER } = require('../common/helpers/constants');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const ChunkManifestPlugin = require('chunk-manifest-webpack-plugin');
const WebpackMd5Hash = require('webpack-md5-hash');
const _ = require('lodash');
const os = require('os').networkInterfaces();
const SERVER_ADDRESS = _.chain(os)
    .values()
    .flatten()
    .filter((address) => (address.family === 'IPv4' && address.internal === false))
    .map('address')
    .head()
    .value();

module.exports = function (env) {
    const isProd = (env.hasOwnProperty('production') && env.production === true);
    const isClient = (env.hasOwnProperty('client') && env.client === true);
    let common = [
        new ProgressBarPlugin(),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            minChunks: function (module) {
                return module.context && module.context.indexOf('node_modules') !== -1;
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: isProd,
            mangle: isProd,
            debug: !isProd,
            sourceMap: !isProd
        }),
        new webpack.SourceMapDevToolPlugin({
            filename: '[name].[chunkhash].js.map',
            exclude: ['vendor', 'node_modules']
        }),
        new ExtractTextPlugin({
            filename: 'css/app.[hash].css',
            disable: !isProd,
        }),
        new HtmlWebpackPlugin({
            template: 'public/index.ejs',
            chunksSortMode: 'auto',
            hash: true,
            cache: true,
            SERVER_PORT: process.env.PORT || SERVER.PORT,
            SERVER_ADDRESS: isClient ? SERVER_ADDRESS : '127.0.0.1'
        })
    ];
    return isProd ? common : Array.prototype.concat(common, [
        new webpack.optimize.CommonsChunkPlugin({
            name: 'manifest'
        }),
        new WebpackMd5Hash(),
        new ManifestPlugin(),
        new ChunkManifestPlugin({
            filename: "chunk-manifest.json",
            manifestVariable: "webpackManifest"
        }),
        new webpack.optimize.OccurrenceOrderPlugin()
    ]);
};
