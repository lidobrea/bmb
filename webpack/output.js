const path = require('path');
const ROOT = path.join(path.dirname(__filename), '/../');
module.exports = function (env) {
    const isProd = (env.hasOwnProperty('production') && env['production'] === true);
    return {
        path: path.join(ROOT, 'dist'),
        filename: isProd ? '[name].js' : '[name].[chunkhash].js',
        chunkFilename: '[name].[chunkhash].js'
    }
};