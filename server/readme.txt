Chart API

API-ul de grafic indeplineste doua functionalitati majore:

1. Modul pentru citirea, procesarea si trimiterea catre clienti a datelor primite in format .csv in directorul 
'assets/static'. Trimiterea datelor procesate catre aplicatia web se face prin websockets.
Datele trebuie sa respecte un format standard, definit in fisierul IIndex.json din directorul 'common/entities'.
Procesarea datelor consta in parsarea fisierului .csv si transformarea si maparea acestuia in obiect JSON.

2. Modul de interpretare (citire, intepretare, stergere) a unor operatiuni (comenzi) si statusi.
Aceste comenzi intermediaza comunicarea intre aplicatia web si o alta aplicatie externa. Comenzile sunt trimise
intr-un fisier sub format .txt, stocate in directorul 'commands'.

Aplicatia web are 11 butoane (comenzi) definite in 2 seturi (CONNECT, NORMAL TRADING, BUY s.a.m.d). La apasarea unui
buton se va trimite catre server prin websocket o comanda de genul '12' in fisierul command.txt din dir 'commands/Order'.
Primul caracter reprezinta setul din care a fost trimisa comanda (1 sau 2).
Al 2-lea caracter reprezinta instructiunea din setul respectiv (1-7 pentru primul set, 1-4 pentru al 2-lea et).
De exemplu: apasarea butonului CONNECT (care apartine setului 1, si este instructiunea cu numarul 1 din acest set), va
crea un fisier command.txt cu textul '11'. Fisierul se salveaza in dir 'commands/Order'.

Statusii sunt interpretati din fisierul status.txt, care trebuie pus in dir 'commands/OrderStatus'.
Un status corect are atatea caractere cate butoane exista in aplicatia web (in cazul nostru, 11). Fiecare buton are un 
corespondent binar de 1 bit.
De exemplu, statusul 00000000000 reprezinta un starea 'DISABLED' pentru toate butoanele din aplicatie.
Tot in fisierul status.txt, se poate trimite si ultima comanda efectuata.
Pot fi trei comenzi, SHORT (10), LONG (01) si NEUTRAL (00). Acestea se trimit pe al 2-lea rand al fisierului status.txt

Un exemplu complet al fisierului status.txt ar putea fi:
10000010000
00

3. Serverul mai suporta si intepretarea fisierelor .csv uploadate din interfata aplicatie web.
In cazul acesta, procesarea datelor se realizeaza ca la punctul 1. 
Fisierul se urca prin metoda HTTP POST la endpoint-ul '/api/upload', urmand ca serverul sa returneze o colectie de date procesate,
pentru a putea fi afisate sub forma de grafic.

Aplicatia web

Aplicatia web are o interfata simpla cu butoane si un grafic.
Aplicatia web asculta prin websocket la primirea datelor pe care apoi le proceseaza in formatul acceptat de libraria CanvasJS
Aplicatia web poate trimite prin websocket comenzi catre API prin apasarea butoanelor din setul "Commands".
Graficul suporta cateva functionalitati de zoom, pinch, move, reset.

Modul testare

In fisierul csvFactory.js din folderul scripts este un modul care genereaza date random in ce respecta formatul
IIndex.json si le salveaza in assets/static. Acesta se foloseste pentru testare utilizand comanda
node scripts/csvFactory --limit 100 (sau orice alt numar de fisiere)