const express = require('express');
const Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs'));
const bodyParser = require('body-parser');
const csvParse = Promise.promisify(require('csv-parse'));
const _ = require('lodash');
const IIndex = require('../common/entities/IIndex.json');
const moment = require('moment');
const path = require('path');
const chokidar = require('chokidar');
const app = express();
const api = express.Router();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const rimraf = require('rimraf');
const ROOT = path.join(path.dirname(require.main.filename), '/../');
const ASSETS_PATH = path.join(ROOT, 'assets/static');
const COMMANDS_PATH = path.join(ROOT, 'commands');
const csvOpts = { columns: true, relax_column_count: true, trim: true, auto_parse: true };
const { INDEX, SOCKET, FILENAMES, DATE_FORMATS, EVENTS, SERVER, ACTIONS } = require('../common/helpers/constants');
let clients = {};
let existingData = [];
let currentDay = moment().format(DATE_FORMATS.L);
const isClient = (process.env.hasOwnProperty('CLIENT') && process.env['CLIENT'] == 'true');
const os = require('os').networkInterfaces();
const SERVER_ADDRESS = isClient ? _.chain(os)
    .values()
    .flatten()
    .filter((address) => (address.family === 'IPv4' && address.internal === false))
    .map('address')
    .head()
    .value() : '127.0.0.1';

server.listen(SERVER.PORT, SERVER_ADDRESS, () => {
    console.log(`Server started at ${server.address().address}:${server.address().port}`);
});

app.use(bodyParser.json({ limit: '2.5mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '2.5mb' }));
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use('/api', api);

api.post('/upload', (req, res) => {
    asyncFileHandler(Promise.resolve(req.body.data))
        .then((data) => res.json(data))
        .catch((err) => res.status(500).send(err));
});
api.get('/dayData', (req, res) => {
    if (existingData.length) {
        res.json(existingData);
    } else {
        res.status(500).send('No available data');
    }
});

app.set('view engine', 'ejs');
app.use('/', express.static(path.join(ROOT, 'dist')));

io.on(SOCKET.ON_CONNECT, (client) => {
    const clientId = client.client.id;

    if (!clients[clientId]) {
        clients[clientId] = client;
        console.info(`Client ${clientId} connected`);
        if (existingData.length) {
            notifyClients(SOCKET.ON_DATA_REQUEST, existingData);
        }
    }

    client.on(SOCKET.ON_COMMAND, (commandId) => {
        let fileExists = fs.existsSync(path.join(COMMANDS_PATH, 'Order/command.txt'));
        if (fileExists) {
            console.warn('Command not yet processed by TWS');
        } else {
            notifyClients(SOCKET.STATUS, {
                statuses: _.reduce(Array.from({ length: 11 }, () => 0),
                    (result, value, idx) => {
                        let objKey = idx <= 6 ? `1${idx + 1}` : `2${idx - 6}`;
                        result[objKey] = 'disabled';
                        return result;
                    }, {}),
                action: ''
            });
            fs.writeFile(path.join(COMMANDS_PATH, 'Order/command.txt'), commandId);
        }
    });

    client.on(SOCKET.ON_DISCONNECT, () => {
        console.info(`Client ${clientId} disconnected`);
        delete clients[clientId];
    });
});

fs.readdirAsync(ASSETS_PATH).then((files) => {
    let fileNames = _.chain(files)
        .filter(fileName => new RegExp(FILENAMES.GRAFIC).test(fileName))
        .sortBy(fileName => parseInt(fileName.match(/\d+/g)))
        .value();

    return Promise.all(Array.from(fileNames, (fileName) =>
        asyncFileHandler(fs.readFileAsync(`${ASSETS_PATH}/${fileName}`, 'utf8')).then(data => _.flatMap(data))
    ));
}).then((data) => {
    data = _.flatMap(data);
    if (!data.length) {
        console.log('No initial data');
        return;
    }
    existingData = data;
});

chokidar.watch(path.join(COMMANDS_PATH, 'OrderStatus'), {
    ignored: '.gitkeep',
    usePolling: true
}).on(EVENTS.ADD, readStatus);

chokidar.watch(path.join(COMMANDS_PATH, 'OrderStatus'), {
    ignoreInitial: true,
    ignored: '.gitkeep'
}).on(EVENTS.CHANGE, readStatus);

/**
 * Reads, parses and interprets a .txt file containing numeric characters on 2 rows
 * @param {string} filePath
 */
function readStatus(filePath) {
    if (!/status/.test(filePath)) return;
    fs.openAsync(filePath, 'rs+')
        .then((fd) => {
            fs.readFileAsync(fd, 'utf8').then((rawFile) => {
                fs.closeSync(fd);
                let action;
                let fileContents = _.chain(rawFile)
                    .split(/\\r?\\n|\\n|\r?\n|\n/)
                    .map((contents) => contents.trim().replace(/\r|\n/g, ''))
                    .value();
                let rawStatuses = fileContents[0].split('');
                let rawAction = fileContents[1];

                let statuses = _.reduce(rawStatuses, (result, value, idx) => {
                    let status = parseInt(value) ? '' : 'disabled';
                    let objKey = idx <= 6 ? `1${idx + 1}` : `2${idx - 6}`;
                    result[objKey] = status;
                    return result;
                }, {});
                switch (rawAction) {
                    case '00':
                        action = ACTIONS.NEUTRAL;
                        break;
                    case '10':
                        action = ACTIONS.SHORT;
                        break;
                    case '01':
                        action = ACTIONS.LONG;
                        break;
                    default:
                        action = '';
                        break;
                }

                notifyClients(SOCKET.STATUS, { statuses, action });
                
                rimraf(filePath, {maxBusyTries: 10}, (err) => {
                    if (err) return console.error(`Failed deleting file because of ${err}`);
                });
            })
    }).catch((err) => {
        console.error(`Cannot read ${filePath} because of ${err}`);
    });
}

let assetWatcher = chokidar.watch(ASSETS_PATH, { ignoreInitial: true, ignored: '.gitkeep' });

assetWatcher.on(EVENTS.REMOVE, _.debounce((filePath) => {
    if (!new RegExp(FILENAMES.GRAFIC).test(filePath)) return;
    console.info(`Data for ${currentDay} deleted`);
    currentDay = moment().format(DATE_FORMATS.L);
    existingData = [];
    notifyClients(SOCKET.ON_DATA_REMOVED);
}, 250));

assetWatcher.on(EVENTS.ADD, (filePath) => {
    asyncFileHandler(fs.readFileAsync(filePath, 'utf8'))
        .then((data) => {
            if (!data.length) {
                console.error('No data available');
                return;
            }
            data = _.flatMap(data);
            existingData = Array.prototype.concat.call(existingData, data);
            notifyClients(SOCKET.ON_DATA, data);
        }).catch((err) => console.error(err));
});

/**
 * Processes CSV data
 * @param {IIndex[]} data
 * @returns IIndex[]
 */
let processData = function (data) {
    return _.map(data, (Index) => {
        let date = moment(Index[INDEX.DATE], DATE_FORMATS.LTS);
        Index[INDEX.DATE] = date.format(DATE_FORMATS.LTS);
        Index[INDEX.ISO_DATE] = date.valueOf();
        return Index;
    });
};

/**
 * Processes .csv file
 * @param {Promise} filePromise
 * @returns {Promise<IIndex[]>}
 */
let asyncFileHandler = function (filePromise) {
    return filePromise
        .then((fileData) => csvParse(fileData, csvOpts))
        .then((data) => processData(Array.prototype.concat(data)))
        .catch((err) => console.error(err));
};

/** Notifies Clients through websocket
 * @param {string} eventName
 * @param data?
 */
let notifyClients = function (eventName, data) {
    for (let i in clients) {
        clients[i].emit(eventName, data);
    }
}