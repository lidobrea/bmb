import 'Libs/canvasjs.min';
import moment from 'moment';
import $ from 'jquery';
import * as _ from 'lodash';

import { DATE_FORMATS, CHART } from 'Common/helpers/constants';
import canvasjsOpts from 'Client/src/chart/defaults/canvasjsOpts';
import IChart from 'Client/src/entities/IChart';

export default function (elId, data, opts) {
    data = Array.prototype.concat.call(data);
    _.map(canvasjsOpts.data, (defaultData, idx) => {
        defaultData.dataPoints = data[idx] || [];
    });

    let _chartInstance = new CanvasJS.Chart(elId, _.merge(_.clone(canvasjsOpts), opts));
    let _isZoomed, _lastZoom, _lastPoint = {};

    function CanvasAdapter(opts) { }

    CanvasAdapter.prototype = Object.create(IChart.prototype);

    _chartInstance.options.rangeChanged = function (e) {
        _isZoomed = false;
        $(`#${elId}`).trigger('rangeChanged');
    };

    CanvasAdapter.prototype = {
        constructor: CanvasAdapter,
        setZoom: function (value) {
            let minValue = new Date(_chartInstance.axisX[0].get('maximum'));
            _lastZoom = value;
            _isZoomed = true;
            switch (value) {
                case CHART.ZOOM.M1:
                    minValue.setMinutes(minValue.getMinutes() - 1);
                    break;
                case CHART.ZOOM.M5:
                    minValue.setMinutes(minValue.getMinutes() - 5);
                    break;
                case CHART.ZOOM.M30:
                    minValue.setMinutes(minValue.getMinutes() - 30);
                    break;
                case CHART.ZOOM.H1:
                    minValue.setHours(minValue.getHours() - 1);
                    break;
                default:
                    this.resetZoom();
                    return;
                    break;
            }
            _chartInstance.axisX[0].set('viewportMinimum', minValue.valueOf());
        },
        resetZoom: function () {
            _isZoomed = false;
            _chartInstance.axisX[0].set('viewportMinimum', 'auto');
            _chartInstance.axisX[0].set('viewportMaximum', 'auto');
        },
        addData: function (dataPoints) {
            let dataSets = _chartInstance.options.data;
            for (let i = 0; i < dataSets.length; i++) {
                let dataSet = dataSets[i];
                _lastPoint[i] = dataSet.dataPoints[dataSet.dataPoints.length - 1];
                if (_lastPoint[i] && dataPoints[i].x < _lastPoint[i].x) {
                    console.warn(`You are trying to add a date (${moment(dataPoints[i].x).format(DATE_FORMATS.LTS)})
                    that happened sometime before the last date in the current dataset 
                     (${moment(_lastPoint[i].x).format(DATE_FORMATS.LTS)})`);
                    try {
                        let idx = _getOptimalIndex(_.map(dataSet.dataPoints, 'x'), _lastPoint[i].x);
                        dataSet.dataPoints.splice(idx, 0, dataPoints[i]);
                    } catch(e) {
                        console.warn(e);
                    }
                } else {
                    dataSet.dataPoints.push(dataPoints[i]);
                }
            }
        },
        updateData: function (dataCollection) {
            let dataSets = _chartInstance.options.data;
            for (let i = 0; i < dataSets.length; i++) {
                dataSets[i].dataPoints = dataCollection[i];
            }
        },
        destroyData: function () {
            let dataSets = _chartInstance.options.data;
            for (let i = 0; i < dataSets.length; i++) {
                dataSets[i].dataPoints = [];
            }
        },
        setAxisInterval: function (min, max) {
            if (!min && !max) return;
            if (isNaN(min) || isNaN(max)) {
                console.error(`Cannot set range for ${min} - ${max}`);
                return;
            }

            min = parseInt(min);
            min = Math.max(0, min - (min * 0.015));
            max = parseInt(max);
            max = max + max * 0.015;

            _chartInstance.axisY[0].set('minimum', min);
            _chartInstance.axisY[0].set('maximum', max);
        },
        repaint: function() {
            if (canvasjsOpts.keepZoom && _isZoomed) {
                this.setZoom(_lastZoom);
            }
            _chartInstance.render();
        },
        get instance() {
            return _chartInstance;
        },
        get data() {
            return _chartInstance.get('data');
        }
    };

    function _getOptimalIndex(comparisonSet, searchElement) {

            let minIndex = 0;
            let maxIndex = comparisonSet.length - 1;
            let currentIndex;
            let currentElement;

            while (minIndex <= maxIndex) {
                currentIndex = (minIndex + maxIndex) / 2 | 0;
                currentElement = comparisonSet[currentIndex];
                if (currentElement < searchElement) {
                    minIndex = currentIndex + 1;
                }
                else if (currentElement > searchElement) {
                    maxIndex = currentIndex - 1;
                }
                else {
                    throw Error(`Item already exists at index: ${currentIndex}`);
                }
            }

            return currentIndex;
    }

    return new CanvasAdapter(opts);
}