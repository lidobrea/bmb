import plotlyOpts from '../defaults/plotlyjsOpts';

export default (elId, data, opts) => Plotly.newPlot(elId, data, merge(plotlyOpts, opts));
