import highchartOpts from '../defaults/highchartOpts';

export default (elId, data, opts) => Highstock.stockChart(elId, merge(highchartOpts, opts));
