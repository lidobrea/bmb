import ChartAdapter from './adapters/canvasjsAdapter';
import IChart from 'Client/src/entities/IChart';

export default function Chart(elId, data, opts) {
    let adapter = new ChartAdapter(elId, data, opts);

    function Chart(data, opts) { }

    Chart.prototype = Object.create(IChart.prototype);

    Chart.prototype = {
        constructor: Chart,
        setZoom: (value) => adapter.setZoom(value),
        destroyData: () => adapter.destroyData(),
        addData: (data) => adapter.addData(data),
        updateData: (data) => adapter.updateData(data),
        resetZoom: () => adapter.resetZoom(),
        setAxisInterval: (min, max) => adapter.setAxisInterval(min, max),
        repaint: () => adapter.repaint(),
        get instance() { return adapter.instance },
        get data() { return adapter.data }
    };

    return new Chart(data, opts);
}