import { COLORS } from 'Common/helpers/constants';

export default {
    axisX: {
        titleFontSize: 16,
        labelFontSize: 14,
        labelFontFamily: 'Open Sans',
        titleFontFamily: 'Open Sans',
        valueFormatString: 'H:m:s:fff',
        labelAngle: -20,
        stripLines: [{
            thickness: 1,
            color: COLORS.WHITE_GREY,
            showOnTop: true
        }]
    },
    axisY: {
        title: 'Exchange rate',
        interlacedColor: "#F0F8FF",
        titleFontSize: 16,
        labelFontSize: 14,
        labelFontFamily: 'Open Sans',
        titleFontFamily: 'Open Sans',
        tickLength: 15,
        gridThickness: 1,
        includeZero: false,
        markerType: 'none',
        markerColor: COLORS.CHART,
        stripLines: [{
            thickness: 1,
            color: COLORS.WHITE_GREY,
            showOnTop: true
        }]
    },
    keepZoom: true,
    toolTip: {
        fontSize: 12,
        borderColor: COLORS.CHART,
        fontFamily: 'Open Sans',
        fontStyle: 'normal',
        borderThickness: 1,
        backgroundColor: 'rgba(247,247,247,0.85)',
        cornerRadius: 3,
        contentFormatter: (e) => {
            let content;
            if (e.entries[0].dataPoint.summary) {
                content = e.entries[0].dataPoint.summary;
            } else {
                content = `${e.entries[0].dataPoint.date}<br/>Exchange: ${e.entries[0].dataPoint.y}`;
            }
            e.chart.currentX = e.entries[0].dataPoint.x;
            e.chart.currentY = e.entries[0].dataPoint.y;
            return content;
        },
        shared: true
    },
    zoomEnabled: true,
    data: [{
        id: 'default',
        type: 'line',
        lineColor: COLORS.CHART,
        xValueType: 'dateTime',
        dataPoints: [],
        indexLabelFontFamily: 'Open Sans',
        indexLabelLineColor: COLORS.BLACK,
        indexLabelLineThickness: 1,
        indexLabelFontSize: 13,
        indexLabelFontColor: COLORS.BLACK,
        connectNullData: true
    }]
}