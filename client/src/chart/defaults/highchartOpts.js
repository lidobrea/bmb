export default {
    // chart: {
    //     heigth: 400,
    // },
    title: {
        text: 'BMB'
    },
    subtitle: {
        text: 'Real time data'
    },
    exporting: {
        enabled: true
    },
    credits: {
        enabled: false
    },
    plotOptions: {
        line: {
            gapSize: 90000
        }
    },
    rangeSelector: {
        allButtonsEnabled: true,
        buttons: [{
            count: 1,
            type: 'minute',
            text: '1M'
        }, {
            count: 5,
            type: 'minute',
            text: '5M'
        }, {
            count: 30,
            type: 'minute',
            text: '30M',
            dataGrouping: {
                forced: true,
                units: [['minute', [30]]]
            }
        }, {
            count: 1,
            type: 'hour',
            text: '1H',
            dataGrouping: {
                forced: true,
                units: [['hour', [1]]]
            }
        }, {
            count: 1,
            type: 'day',
            text: '1D',
            dataGrouping: {
                units: [['day', [1]]]
            }
        }, {
            type: 'all',
            text: 'All'
        }],
        inputEnabled: false,
        selected: 1,
        buttonTheme: {
            width: 50
        }
    },
    tooltip: {
        style: {
            padding: 0
        },
        shadow: false,
        backgroundColor: '#FFF',
        // crosshairs: true,
        useHTML: true,
        valueDecimals: 4,
        shared: true
    },
    yAxis: {
        title: {
            text: 'Exchange rate'
        },
        crosshair: {
            className: "highcharts-crosshair",
            snap: false
        }
    },
    series: [{
        name: 'Exchange',
        id: 'dataSeries',
        connectNulls: true
    }, {
        type: 'flags',
        id: 'flagSeries',
        onSeries: 'dataSeries',
        shape: 'squarepin'
    }]
}