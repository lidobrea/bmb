import '../assets/styles/main.less';

import $ from 'jquery';
import * as io from 'socket.io-client';
import * as _ from 'lodash';
import request from 'request-promise';

import { SOCKET, INDEX, DEBOUNCE_RATE, COLORS } from 'Common/helpers/constants';
import I18N from 'Common/helpers/i18n';
import Chart from './chart/chart';

// Elements
const endDateEl = document.getElementById('indexEndTime');
const chartStatusEl = document.getElementById('chartStatus');
const socketStatusEl = document.getElementById('socketStatus');
const highValueEl = document.getElementById('indexHigh');
const lowValueEl = document.getElementById('indexLow');
const lastValueEl = document.getElementById('indexLast');
const lastActionEl = document.getElementById('lastAction');
let chartContainerEl = document.getElementById('chartEl');

// Hooks
const WSconnectBtn = document.getElementById('hook-connect');
const WSdisconnectBtn = document.getElementById('hook-disconnect');
const requestDataBtn = document.getElementById('hook-requestData');
const $commandBtns = $('.hook-sendCommand');
const uploadBtn = document.getElementById('hook-uploadBtn');
const uploadInputEl = document.getElementById('upload');
const chartDestroyBtn = document.getElementById('hook-destoryChart');
const zoomChartBtn = $('.hook-zoomChart');

const SERVER_URL = `http://${SERVER_ADDRESS}:${SERVER_PORT}`;
const API = `${SERVER_URL}/api`;
const socketCfg = {
    url: SERVER_URL,
    opts: { reconnection: true, autoConnect: true, timeout: 25000 }
};
let chartYAxis = { min: undefined, max: undefined };

const socket = io.connect(socketCfg.url, socketCfg.opts);

let chart = new Chart('chartEl', [], {});
let chartInstance = chart.instance;
chart.repaint();

socket.on(SOCKET.ON_CONNECT, () => {
    console.info(`WS: Connected to server ${socketCfg.url}`);
    socketStatusEl.innerText = 'CONNECTED';
    socketStatusEl.classList.add('connected');
    socketStatusEl.classList.remove('disconnected');
    WSconnectBtn.classList.add('disabled');
    WSdisconnectBtn.classList.remove('disabled');
    requestDataBtn.classList.add('disabled');
    $($commandBtns[0]).removeClass('disabled').removeAttr('disabled');
    $($commandBtns[6]).removeClass('disabled').removeAttr('disabled');
    uploadBtn.classList.add('disabled');
});
socket.on(SOCKET.ON_DISCONNECT, () => {
    console.info(`WS: Disconnected from server ${socketCfg.url}`);
    socketStatusEl.innerText = 'DISCONNECTED';
    socketStatusEl.classList.add('disconnected');
    socketStatusEl.classList.remove('connected');
    WSconnectBtn.classList.remove('disabled');
    WSdisconnectBtn.classList.add('disabled');
    requestDataBtn.classList.remove('disabled');
    $commandBtns.addClass('disabled').attr('disabled');
    uploadBtn.classList.remove('disabled');
});
socket.on(SOCKET.STATUS, ({ statuses, action }) => {
    lastActionEl.innerText = action;
    _.forEach(statuses, (status, idx) => {
        let el = document.querySelector(`[command-id="${idx}"]`);
        el.classList.remove('disabled');
        el.removeAttribute('disabled');
        if (!!status) {
            el.classList.add(status);
            el.setAttribute(status, status);
        }
    });
});
socket.on(SOCKET.ON_DATA, (data) => addPoint(data));
socket.on(SOCKET.ON_DATA_REQUEST, (data) => updateChart(data));
socket.on(SOCKET.ON_DATA_REMOVED, () => destroyChart(I18N.DATA_REMOVED));
console.timeEnd('newPoint');

let addPoint = function (IndexCollection) {
    if (!IndexCollection.length) return;
    chartStatusEl.innerText = '';
    console.time('New Point');
    _.map(IndexCollection, (Index) => {
        let point = Object.assign({
            x: Index[INDEX.ISO_DATE],
            y: Index[INDEX.MID_PRICE],
            date: Index[INDEX.DATE]
        }, buildIndexMarker(Index));
        chartYAxis = {
            min: point.y >= chartYAxis.min ? chartYAxis.min : point.y,
            max: point.y <= chartYAxis.max ? chartYAxis.max : point.y
        };
        updateChartStatus(Index[INDEX.DATE], Index[INDEX.MID_PRICE], Index[INDEX.ACTION]);
        chart.addData([point]);
    });
    throttledChartRepaint();
};
let throttledChartRepaint = _.throttle(() => {
    chart.repaint();
    console.timeEnd('New Point');
}, DEBOUNCE_RATE, { leading: true, trailing: false });
let debouncedChartRepaint = _.debounce(function () {
    chart.repaint();
    // console.timeEnd('New Point');
}, DEBOUNCE_RATE, { leading: true, trailing: false });
let updateChart = function (IndexCollection) {
    if (!IndexCollection.length) return;
    chartStatusEl.innerText = '';
    console.time('chartPopulate');
    IndexCollection = _.map(IndexCollection, (Index) => {
        if (!Index) return;
        let point = Object.assign({
            x: Index[INDEX.ISO_DATE],
            y: Index[INDEX.MID_PRICE],
            date: Index[INDEX.DATE]
        }, buildIndexMarker(Index));
        chartYAxis = {
            min: point.y >= chartYAxis.min ? chartYAxis.min : point.y,
            max: point.y <= chartYAxis.max ? chartYAxis.max : point.y
        };
        updateChartStatus(Index[INDEX.DATE], Index[INDEX.MID_PRICE], Index[INDEX.ACTION]);

        return point;
    });
    chart.updateData([IndexCollection]);
    chart.repaint();
    console.timeEnd('chartPopulate');
};
let buildIndexSummary = function (Index) {
    let result = ``;
    _.map(Index, (value, key) => {
        if (value !== '' && key !== INDEX.ISO_DATE && key) {
            result += `${key}: ${value}<br/>`;
        }
    });
    return result;
};
let buildIndexMarker = function (Index) {
    let label = '', color = '', textColor = COLORS.BLACK, size = 14, type = 'triangle';

    if (!Index[INDEX.MOTIVATION]) {
        return { markerColor: COLORS.CHART, markerType: 'none' };
    }

    if (Index[INDEX.STATE] === INDEX.STATES.LONG)
        label = INDEX.MARKERS.TAGS.L;
    else
        label = INDEX.MARKERS.TAGS.S;
    if (Index[INDEX.ACTION] === INDEX.ACTIONS.BUY)
        label += INDEX.MARKERS.TAGS.B;
    else
        label += INDEX.MARKERS.TAGS.S;

    if (new RegExp(INDEX.MOTIVATIONS.TURA).test(Index[INDEX.MOTIVATION])) {
        color = COLORS.BLUE;
        textColor = COLORS.WHITE;
        label = INDEX.MARKERS.TAGS.TURA;
    }
    if (new RegExp(INDEX.MOTIVATIONS.IN).test(Index[INDEX.MOTIVATION])) {
        color = COLORS.WHITE_GREY;
        label = INDEX.MARKERS.TAGS.SIMULAT;
    }
    if (new RegExp(INDEX.MOTIVATIONS.OUT).test(Index[INDEX.MOTIVATION])) {
        color = COLORS.DARK_GREY;
        label = INDEX.MARKERS.TAGS.REAL;
    }

    switch (Index[INDEX.MOTIVATION]) {
        case INDEX.MOTIVATIONS.STANDBY:
            color = COLORS.WHITE;
            break;
        case INDEX.MOTIVATIONS.PROTECTS:
            color = COLORS.YELLOW;
            break;
        case INDEX.MOTIVATIONS.FAST_TAKE_PROFIT:
            color = COLORS.GREEN;
            break;
        case INDEX.MOTIVATIONS.TAKE_PROFIT:
            color = COLORS.GREEN;
            break;
        case INDEX.MOTIVATIONS.BOUNCE_BACK:
        case INDEX.MOTIVATIONS.FAST_BOUNCE_BACK:
            color = COLORS.WHITE;
            break;
        case INDEX.MOTIVATIONS.FAST_CORRECTION:
        case INDEX.MOTIVATIONS.FAST_STOP_LOSS:
            color = COLORS.ORANGE;
            break;
        case INDEX.STOP_LOSS:
        case INDEX.MOTIVATIONS.STOPLOSS:
            color = COLORS.RED;
            break;
        case INDEX.MOTIVATIONS.CLOSURE:
            color = COLORS.WHITE;
            break;
        case INDEX.MOTIVATIONS.DOUBLE:
        case INDEX.MOTIVATIONS.DOUBLE_EMPTY:
            color = COLORS.LIGHT_YELLOW;
            break;
        case INDEX.MOTIVATIONS.CANCEL:
            color = COLORS.RED_BLACK;
            textColor = COLORS.WHITE;
            size = 14;
            break;
    }

    return {
        indexLabel: label,
        indexLabelBackgroundColor: color,
        indexLabelFontColor: textColor,
        summary: buildIndexSummary(Index),
        markerType: type
    };
};
let updateChartStatus = function (date, price, status) {
    chartStatusEl.innerText = status || '';
    endDateEl.innerText = date || '';
    highValueEl.innerText = !isNaN(chartYAxis.max) ? chartYAxis.max.toFixed(3) : '';
    lowValueEl.innerText = !isNaN(chartYAxis.min) ? chartYAxis.min.toFixed(3) : '';
    lastValueEl.innerText = !isNaN(parseInt(price)) ? price.toFixed(3) : '';
};
let destroyChart = function (reason) {
    if (typeof reason !== 'string') {
        reason = null;
    }
    updateChartStatus(null, null, reason);
    chartContainerEl.querySelector("button[state='reset']").click();
    chart = new Chart('chartEl', [], {});
    chart.repaint();
    chartInstance = chart.instance;
    chartContainerEl.querySelector("button[state='reset']").addEventListener('click', () => {
        zoomChartBtn.removeClass('selected');
        chart.resetZoom();
    });
};
let fileUploadHandler = function uploadFile(e) {
    if (e.target.files.length > 0) {
        let fileHandle = e.target.files[0];
        let reader = new FileReader();
        reader.onloadend = (file) => {
            chartStatusEl.innerText = I18N.LOADING_STATIC;
            request({
                method: 'POST',
                uri: `${API}/upload`,
                body: { data: file.target.result },
                json: true
            }).then((data) => {
                chart.destroyData();
                updateChart(data);
                uploadInputEl.value = '';
            }).catch((err) => {
                chartStatusEl.innerText = err.message;
                console.error(err);
            });
        };
        reader.readAsText(fileHandle);
    }
};

/* Events */

chartContainerEl.onmousemove = () => {
    if (chart.prevXData !== chartInstance.currentX || chart.prevYData !== chartInstance.currentY) {
        chartInstance.axisX[0].stripLines[0].set('value', chartInstance.currentX);
        chartInstance.axisY[0].stripLines[0].set('value', chartInstance.currentY);
        // chartInstance.options.axisX.stripLines[0].value = chartInstance.currentX;
        // chartInstance.options.axisY.stripLines[0].value = chartInstance.currentY;
        chart.prevXData = chartInstance.currentX;
        chart.prevYData = chartInstance.currentY;
        // chart.repaint();
    }
};
chartContainerEl.onmouseleave = () => {
    chartInstance.axisX[0].stripLines[0].set('value', undefined);
    chartInstance.axisY[0].stripLines[0].set('value', undefined);
    // chartInstance.options.axisX.stripLines[0].value = undefined;
    // chartInstance.options.axisY.stripLines[0].value = undefined;
    // chart.repaint();
};
// Socket
WSconnectBtn.addEventListener('click', (e) => {
    e.preventDefault();
    socket.connect(socketCfg.url, socketCfg.opts);
});
WSdisconnectBtn.addEventListener('click', (e) => {
    e.preventDefault();
    socket.close();
});
// Chart
requestDataBtn.addEventListener('click', (e) => {
    e.preventDefault();
    requestDataBtn.classList.add('disabled');
    uploadBtn.classList.add('disabled');
    request({
        method: 'GET',
        uri: `${API}/dayData`,
        json: true
    }).then((data) => {
        requestDataBtn.classList.remove('disabled');
        uploadBtn.classList.remove('disabled');
        chart.destroyData();
        updateChart(data);
    }).catch((err) => {
        console.error(err);
        chartStatusEl.innerText = err.message;
        requestDataBtn.classList.remove('disabled');
        uploadBtn.classList.remove('disabled');
    });
});
chartDestroyBtn.addEventListener('click', destroyChart);
uploadBtn.addEventListener('click', (e) => {
    e.preventDefault();
    uploadInputEl.click();
});
uploadInputEl.addEventListener('change', fileUploadHandler);
zoomChartBtn.on('click', (e) => {
    e.preventDefault();
    let el = e.target;
    let zoomArg = el.getAttribute('zoom');
    chartContainerEl.querySelector("button[state='reset']").click();
    el.classList.add('selected');
    chart.setZoom(zoomArg);
});
$(chartContainerEl).on('rangeChanged', () => {
    zoomChartBtn.removeClass('selected');
});
// Commands
$commandBtns.on('click', (e) => {
    e.preventDefault();
    socket.emit(SOCKET.ON_COMMAND, e.target.getAttribute('command-id'));
    $commandBtns.addClass('disabled').attr('disabled');
});
chartContainerEl.querySelector("button[state='reset']").addEventListener('click', () => {
    zoomChartBtn.removeClass('selected');
    chart.resetZoom();
});