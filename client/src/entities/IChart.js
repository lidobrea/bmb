export default function IChart(elId, data, options) {}
/**
 * Set chart zoom
 * @param values
 */
IChart.prototype.setZoom = function (values) {
    throw new Error('method not implemented');
};
IChart.prototype.resetZoom = function () {
    throw new Error('method not implemented');
};
IChart.prototype.repaint = function () {
    throw new Error('method not implemented');
};
IChart.prototype.destroyData = function () {
    throw new Error('method not implemented');
};
IChart.prototype.addData = function (data) {
    throw new Error('method not implemented');
};
IChart.prototype.updateData = function (data) {
    throw new Error('method not implemented');
};
/**
 * Reset chart Y axis interval
 * @param {number} min
 * @param {number} max
 */
IChart.prototype.setAxisInterval = function (min, max) {
    throw new Error('method not implemented');
};
IChart.instance = {
    get: function () {
        return this.instance
    }
};