# BRM Realtime Chart

## Author
[Liviu Dobrea](dobrealiviuc@gmail.com)

## Description
Simple data visualization webapp using canvasjs lib and node.js.
node.js proxy is used both interpretor and receiver / provider for the webapp client.

## Prerequisites

Install [node.js](nodejs.org) (minimum version 4.2.0) and [Yarn package manager](https://yarnpkg.com/en/)

## Installation 

1. ```npm install``` or ```yarn install```
2. ```npm run start```

## Usage 

Running ```npm start``` will start a node.js on port 3000 that exposes some static files on the current host machine.
You can view the web app in your browser at **serverhost:3000**, where serverhost is your local machine address
(e.g. 192.168.0.100).

## Debugging

Manually run 

1. ```npm run server``` (will start a node.js instance running on localhost:8080)
2. ```npm run webpack-server``` (will start a webpack instance running at localhost:3000)