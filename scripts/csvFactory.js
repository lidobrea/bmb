const path = require('path');
const args = require('minimist')(process.argv.slice(2));
const Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs'));
const csvStringify = Promise.promisify(require('csv-stringify'));
const csvParse = Promise.promisify(require('csv-parse'));
const _ = require('lodash');
const moment = require('moment');
let limit = args['limit'] || 1500;
const intervalTimer = Math.max(isNaN(parseInt(args['interval'])) ? _.random(150, 1000) : args['interval'], 150);
const index = parseInt(args['index']);
const STATIC_PATH = path.join(path.dirname(__filename), '/../assets/static');
const { FILENAMES, DATE_FORMATS, INDEX } = require('../common/helpers/constants');
const FILES_TIMER = `ALL-FILES-${limit}`;
const IIndex = require('../common/entities/IIndex.json');
const csvOpts = { columns: true, relax_column_count: true, trim: true, auto_parse: true };
let lastDate = moment('16:30:00:000', DATE_FORMATS.LTS);
let lastPoint;

fs.readdirAsync(STATIC_PATH)
    .then((files) => _.filter(files, (fileName, idx) => new RegExp(FILENAMES.GRAFIC).test(fileName)))
    .then((files) => {
        files = _.sortBy(files, (fileName) => parseInt(fileName.match(/\d+/g)));
        let lastFile = files[files.length - 1];
        if (lastFile) {
            return fs.readFileAsync(`${STATIC_PATH}/${lastFile}`, 'utf8')
                .then((fileData) => csvParse(fileData, csvOpts))
                .then((parsedData) => {
                    parsedData = _.flatMap(parsedData);
                    lastDate = moment(parsedData[parsedData.length - 1][INDEX.DATE], DATE_FORMATS.LTS);
                    lastPoint = parsedData[parsedData.length - 1][INDEX.MID_PRICE];
                    return files;
                });
        } else {
            return files;
        }
    })
    .then((files) => generateFiles(files.length))
    .catch((err) => console.error(err));

const generateFiles = (startIdx) => {
    let currentIndex = index ? index : startIdx;
    console.time(FILES_TIMER);
    let generateNewFile = setInterval(() => {
        if (!isNaN(limit)) limit--;
        ++currentIndex;
        if (currentIndex <= startIdx) {
            lastDate = lastDate.subtract(intervalTimer * Math.max(startIdx - currentIndex, 1), 'ms');
        } else {
            lastDate = lastDate.add(intervalTimer, 'ms');
        }
        let min = (lastPoint - (lastPoint * 0.0025)) || 302;
        let max = (lastPoint + (lastPoint * 0.0025)) || 306;
        lastPoint = randomizeNumbers(min, max);
        let newIndex = [_.merge(_.clone(IIndex), {
            'OrderId': currentIndex,
            'Data/ora': moment(lastDate).format(DATE_FORMATS.LTS),
            'ISODate': lastDate.valueOf(),
            'Pret mediu': lastPoint,
            'Pret achizitie': randomizeNumbers(min, max),
            'Pret TWS': randomizeNumbers(min, max),
            'BID Price': randomizeNumbers(min, max),
            'ASK Priced': randomizeNumbers(min, max),
            'Spread BID-ASK': randomizeNumbers(0, 1)
        })];

        csvStringify(newIndex, { header: true, columns: Object.keys(IIndex) }).then((csv) => {
            let fileName = `${STATIC_PATH}/grafic_${currentIndex}.csv`;
            fs.writeFile(fileName, csv, 'utf8', () => {
                console.info(`New file generated: grafic_${currentIndex}`);
                if (!limit) console.timeEnd(FILES_TIMER);
            })
        }).catch(err => {
            throw new Error(err)
        });

        if (!limit) {
            clearInterval(generateNewFile);
        }
    }, intervalTimer);
};

const randomizeNumbers = (min, max) => _.random(min, max, true);